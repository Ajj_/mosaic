/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#ifndef MAINCOMPONENT_H_INCLUDED
#define MAINCOMPONENT_H_INCLUDED

#include "../JuceLibraryCode/JuceHeader.h"
#include "Audio.h"
#include "LooperUI.h"


//==============================================================================
/*
    This component lives inside our window, and this is where you should put all
    your controls and content.
*/
class MainComponent   : public Component,
                        public MenuBarModel,
                        public ButtonListener
{
public:
    //==============================================================================
    MainComponent (Audio& audio_);
    ~MainComponent();

    void paint (Graphics&) override;
    void resized() override;
    
    void buttonClicked (Button *button) override;
            
    //MenuBarEnums/Callbacks========================================================
    enum Menus
    {
        FileMenu=0,
        NumMenus
    };
    
    enum FileMenuItems
    {
        AudioPrefs = 1,
        NumFileItems
    };
    
    StringArray getMenuBarNames() override;
    PopupMenu getMenuForIndex (int topLevelMenuIndex, const String& menuName) override;
    void menuItemSelected (int menuItemID, int topLevelMenuIndex) override;

private:
    //==============================================================================
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (MainComponent)
    Audio &audio;
    LooperUI looperUI;
    
    ToggleButton ringmodButton;
    
};


#endif  // MAINCOMPONENT_H_INCLUDED
