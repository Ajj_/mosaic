/*
  ==============================================================================

    Audio.cpp
    Created: 9 Oct 2016 5:59:59pm
    Author:  Alex Jones

  ==============================================================================
*/

#include "Audio.h"

Audio::Audio() 
{
    setAudioChannels(2, 2);
    ringModOnOff = 0;
}

Audio::~Audio()
{
    shutdownAudio();
}

void Audio::prepareToPlay (int samplesPerBlockExpected, double sampleRate)
{
    ringMod.prepareToPlay(samplesPerBlockExpected, sampleRate);
    ringModMod.prepareToPlay(samplesPerBlockExpected, sampleRate);
}

void Audio::releaseResources ()
{
    ringMod.releaseResources();
    ringModMod.releaseResources();
}

void Audio::getNextAudioBlock (const AudioSourceChannelInfo &bufferToFill)
{
    float numSamples;
    float *outP[2];
    outP[0] = bufferToFill.buffer->getWritePointer(0);
    outP[1] = bufferToFill.buffer->getWritePointer(1);
    
    numSamples = bufferToFill.buffer->getNumSamples();
    while (numSamples--)
    {
        *outP[0] = looper.processSample(*outP[0]);
        *outP[1] = *outP[0];
        outP[0]++;
        outP[1]++;
    }
    
    if(ringModOnOff.get())
    {
        ringMod.getNextAudioBlock(bufferToFill);
        ringModMod.getNextAudioBlock(bufferToFill);
    }
}

void Audio::setPlayerPlayState (bool newState)
{
    looper.setPlayState(newState);
}

void Audio::loadPlayerFile()
{
    looper.loadFile();
}


void Audio::setRingMod(int state)
{
    ringModOnOff = state;
}

AudioDeviceManager& Audio::returnDeviceManager ()
{
    return deviceManager;
}