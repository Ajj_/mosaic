/*
  ==============================================================================

    Audio.h
    Created: 9 Oct 2016 5:59:59pm
    Author:  Alex Jones

  ==============================================================================
*/

#ifndef AUDIO_H_INCLUDED
#define AUDIO_H_INCLUDED

#include "../JuceLibraryCode/JuceHeader.h"
#include "Looper.h"
#include "RingMod.h"


class Audio : public AudioAppComponent
{
public:
    /** Constructor */
    Audio();
    
    /** Destructor */
    ~Audio();
    
    Looper& getLooper() {return looper;}
    
    /** Tells the source to prepare for playing. */
    void prepareToPlay (int samplesPerBlockExpected, double sampleRate) override;
    
    /** Allows the source to release anything it no longer needs after playback has stopped. */
    void releaseResources () override;
    
    /** Called repeatedly to fetch subsequent blocks of audio data. */
    void getNextAudioBlock (const AudioSourceChannelInfo &bufferToFill) override;
    
    void setPlayerPlayState (bool newState);
    
    void loadPlayerFile();
    
    /** Sets ringMod effect on or off */
    void setRingMod(int state);

    /** Returns a reference to the device manager */
    AudioDeviceManager& returnDeviceManager ();
    
private:
    Looper looper;
    RingMod ringMod;
    RingMod ringModMod;
    Atomic<int> ringModOnOff;

};


#endif  // AUDIO_H_INCLUDED
