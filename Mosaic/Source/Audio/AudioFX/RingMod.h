/*
  ==============================================================================

    RingMod.h
    Created: 10 Oct 2016 7:14:17pm
    Author:  Alex Jones

  ==============================================================================
*/

#ifndef RINGMOD_H_INCLUDED
#define RINGMOD_H_INCLUDED

#include "../JuceLibraryCode/JuceHeader.h"
/**
 Class for a ring modulator effect
 */

class RingMod : public AudioSource
{
public:
    /** Constructor */
    RingMod();
    
    /** Destructor */
    ~RingMod();
    
    /** Set frequency */
    void setFrequency (float newFreq);
    
    /** Initialise any variables etc */
    void prepareToPlay (int samplesPerBlockExpected, double sampleRate) override;
    
    /** Release resources*/
    void releaseResources () override;
    
    /** Process a buffer of audio */
    void getNextAudioBlock (const AudioSourceChannelInfo &bufferToFill) override;
    
private:
    Atomic<float> frequency;
    float phasePosition[2];
    float sR;
};

#endif  // RINGMOD_H_INCLUDED
