/*
  ==============================================================================

    RingMod.cpp
    Created: 10 Oct 2016 7:14:17pm
    Author:  Alex Jones

  ==============================================================================
*/

#include "RingMod.h"


RingMod::RingMod()
{
    
}

RingMod::~RingMod()
{
    
}

void RingMod::setFrequency (float newFreq)
{
    frequency = newFreq;
}

void RingMod::prepareToPlay (int samplesPerBlockExpected, double sampleRate)
{
    frequency = 200.f;
    phasePosition[0] = 0.f;
    phasePosition[1] = 0.f;
    sR = sampleRate;
}

void RingMod::releaseResources ()
{
    
}

void RingMod::getNextAudioBlock (const AudioSourceChannelInfo &bufferToFill)
{
    const float twoPi = 2 * M_PI;
    const float phaseIncrement = (twoPi * frequency.get())/sR;
    
    for (int ch = 0; ch < bufferToFill.buffer->getNumChannels(); ch++)
    {
        float numSamples = bufferToFill.numSamples;
        float* outP = bufferToFill.buffer->getWritePointer(ch);
        while (numSamples--)
        {
            phasePosition[ch] += phaseIncrement;
            if (phasePosition[ch] > twoPi)
            {
                phasePosition[ch] -= twoPi;
            }
            *outP *= sinf(phasePosition[ch]);
            outP++;
        }
    }
}