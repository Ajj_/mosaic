/*
  ==============================================================================

    TestPlayer.cpp
    Created: 9 Oct 2016 6:57:54pm
    Author:  Alex Jones

  ==============================================================================
*/

#include "Looper.h"

Looper::Looper()
{
    //initialise - not playing / recording
    playState = false;
    recordState = false;
    
    bufferPosition = 0;
    
    audioSampleBuffer.setSize(1, bufferSize);
    audioSampleBuffer.clear();
    
    loopStartPoint = 0;
    loopEndPoint = bufferSize;
        
    loopMarker = -1;
}

Looper::~Looper()
{
    
}

void Looper::setPlayState (const bool newState)
{
    playState = newState;
}

bool Looper::getPlayState () const
{
    return playState.get();
}

void Looper::setRecordState (const bool newState)
{
    recordState = newState;
}

bool Looper::getRecordState () const
{
    return recordState.get();
}

void Looper::recordBuffer()
{
    FileChooser chooser ("Please select a file...", File::getSpecialLocation (File::userDesktopDirectory), "*.wav");
    if (chooser.browseForFileToSave(true))
    {
        File file (chooser.getResult().withFileExtension (".wav"));
        
        OutputStream* outStream = file.createOutputStream();
        
        WavAudioFormat wavFormat;
        
        AudioFormatWriter* writer = wavFormat.
        createWriterFor (outStream, 44100, 1, 16, NULL, 0);
        writer->writeFromAudioSampleBuffer(audioSampleBuffer,
                                           0,
                                           audioSampleBuffer.getNumSamples());
        delete writer;
    }
}

void Looper::loadFile()
{
    AudioFormatManager formatManager; formatManager.registerBasicFormats();
    FileChooser chooser ("Please select the file you want to load...", File::getSpecialLocation (File::userHomeDirectory),
                         formatManager.getWildcardForAllFormats());
    if (chooser.browseForFileToOpen()) {
        File file (chooser.getResult());
        AudioFormatReader* reader = formatManager.createReaderFor (file);
        if (reader != 0)
        {
            audioSampleBuffer.setSize (reader->numChannels, reader->lengthInSamples);
            
            reader->read (&audioSampleBuffer, 0, reader->lengthInSamples, 0, true, false);
            delete reader;
        }
    }
}

void Looper::setLoopPoints(int newLoopMarker)
{
    loopMarker = newLoopMarker;
}

void Looper::reverseBuffer()
{
    audioSampleBuffer.reverse(0, audioSampleBuffer.getNumSamples());
}

void Looper::clearBuffer()
{
    audioSampleBuffer.clear();
}

float Looper::processSample (float input)
{
    float output = 0.f;
    float* outP;
    
    outP = audioSampleBuffer.getWritePointer(0, bufferPosition);
    
    switch (loopMarker.get())
    {
        case 0:
            loopStartPoint = 0;
            loopEndPoint = bufferSize * 0.25;
            break;
        case 1:
            loopStartPoint = bufferSize * 0.25;
            loopEndPoint = bufferSize * 0.5;
            break;
        case 2:
            loopStartPoint = bufferSize * 0.5;
            loopEndPoint = bufferSize * 0.75;
            break;
        case 3:
            loopStartPoint = bufferSize * 0.75;
            loopEndPoint = bufferSize;
            break;
        default:
            loopStartPoint = 0;
            loopEndPoint = bufferSize;
            break;
    }
        
    if (playState.get() == true)
    {
        //play
        output = *outP;
        
        //click 4 times each bufferLength
        if ((bufferPosition % (bufferSize / 8)) == 0)
        {
            output += 0.25f;
        }
        
        //record
        if (recordState.get() == true)
        {
                *outP = input + *outP;
        }
        
        //increment and cycle the buffer counter
        ++bufferPosition;
        
        if (bufferPosition >= loopEndPoint)
        {
            bufferPosition = loopStartPoint;
        }
    }
    return output;
}