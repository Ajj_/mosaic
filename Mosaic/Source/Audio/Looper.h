/*
  ==============================================================================

    TestPlayer.h
    Created: 9 Oct 2016 6:57:54pm
    Author:  Alex Jones

  ==============================================================================
*/

#ifndef TESTPLAYER_H_INCLUDED
#define TESTPLAYER_H_INCLUDED

#include "../JuceLibraryCode/JuceHeader.h"

/**
 Class for an audio looper. records, loops and overdubs a audio input
 */

class Looper
{
public:
    /**
     Constructor - initialise everything
     */
    Looper();
    
    /**
     Destructor
     */
    ~Looper();
    
    /** Starts or stops playback of the looper */
    void setPlayState (bool newState);
    
    /** Gets the current playback state of the looper */
    bool getPlayState() const;
    
    /** Sets/unsets the record state of the looper */
    void setRecordState (bool newState);
    
    /** Gets the current record state of the looper */
    bool getRecordState() const;
    
    /** Load the contents of a file into an audio buffer */
    void loadFile();
    
    /** Defines which area of the buffer to loop */
    void setLoopPoints(int newLoopMarker);
    
    /** Reverses the buffer */
    void reverseBuffer();
    
    /** Clears the buffer */
    void clearBuffer();
    
    /** Processes the audio sample by sample. */
    float processSample (float input);
    
    /** Record the contents of the audio buffer to a file */
    void recordBuffer();
    
private:
    //Shared data
    Atomic<int> recordState;
    Atomic<int> playState;
        
    //Audio data
    static const int bufferSize = 176400; //constant
    unsigned int bufferPosition;
    //    float audioSampleBuffer[bufferSize];
    AudioSampleBuffer audioSampleBuffer;
    
    float loopStartPoint;
    float loopEndPoint;
    
    Atomic<int> loopMarker;
};
#endif  // TESTPLAYER_H_INCLUDED
