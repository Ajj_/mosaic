/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#include "MainComponent.h"


//==============================================================================
MainComponent::MainComponent(Audio& audio_) : audio (audio_), looperUI (audio_.getLooper())
{
    setSize (1100, 700);
    
    addAndMakeVisible(looperUI);
    
    ringmodButton.setButtonText ("Ring Mod");
    addAndMakeVisible (&ringmodButton);
    ringmodButton.addListener (this);
}

MainComponent::~MainComponent()
{
}

void MainComponent::paint (Graphics& g)
{
    g.fillAll (Colours::lightgrey);
}

void MainComponent::resized()
{
    Rectangle<int> r = getLocalBounds();
    
    looperUI.setBounds(r.removeFromTop(200));
    ringmodButton.setBounds(r.removeFromTop(50).removeFromLeft(50));
}

void MainComponent::buttonClicked (Button *button)
{
    if (button == &ringmodButton)
    {
        audio.setRingMod(ringmodButton.getToggleState());
    }
}

//MenuBarCallbacks==============================================================
StringArray MainComponent::getMenuBarNames()
{
    const char* const names[] = { "File", 0 };
    return StringArray (names);
}

PopupMenu MainComponent::getMenuForIndex (int topLevelMenuIndex, const String& menuName)
{
    PopupMenu menu;
    if (topLevelMenuIndex == 0)
        menu.addItem(AudioPrefs, "Audio Prefrences", true, false);
    return menu;
}

void MainComponent::menuItemSelected (int menuItemID, int topLevelMenuIndex)
{
    if (topLevelMenuIndex == FileMenu)
    {
        if (menuItemID == AudioPrefs)
        {
            AudioDeviceSelectorComponent audioSettingsComp (audio.returnDeviceManager(),
                                                            0, 2, 2, 2, true, true, true, false);
            audioSettingsComp.setSize (450, 350);
            DialogWindow::showModalDialog ("Audio Settings",
                                           &audioSettingsComp, this, Colours::lightgrey, true);
        }
    }
}
