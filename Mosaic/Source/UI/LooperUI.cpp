/*
  ==============================================================================

    LooperUI.cpp
    Created: 10 Oct 2016 8:41:04am
    Author:  Alex Jones

  ==============================================================================
*/

#include "../../JuceLibraryCode/JuceHeader.h"
#include "LooperUI.h"

//==============================================================================
LooperUI::LooperUI(Looper& looper_) : looper (looper_)
{
    playButton.setButtonText ("Play");
    playButton.setConnectedEdges(Button::ConnectedOnLeft | Button::ConnectedOnRight);
    playButton.setColour(TextButton::buttonColourId, Colours::grey);
    playButton.setColour(TextButton::buttonOnColourId, Colours::lightgrey);
    addAndMakeVisible (&playButton);
    playButton.addListener (this);
    
    recordButton.setButtonText ("Record");
    recordButton.setConnectedEdges(Button::ConnectedOnLeft | Button::ConnectedOnRight);
    recordButton.setColour(TextButton::buttonColourId, Colours::darkred);
    recordButton.setColour(TextButton::buttonOnColourId, Colours::red);
    addAndMakeVisible (&recordButton);
    recordButton.addListener (this);
    
    saveButton.setButtonText ("Save");
    saveButton.setColour(TextButton::buttonColourId, Colours::grey);
    saveButton.setColour(TextButton::buttonOnColourId, Colours::darkgrey);
    addAndMakeVisible (&saveButton);
    saveButton.addListener (this);
    
    loadButton.setButtonText ("Load");
    loadButton.setColour(TextButton::buttonColourId, Colours::grey);
    loadButton.setColour(TextButton::buttonOnColourId, Colours::darkgrey);
    addAndMakeVisible (&loadButton);
    loadButton.addListener (this);
    
    clearButton.setButtonText ("Clear");
    clearButton.setColour(TextButton::buttonColourId, Colours::grey);
    clearButton.setColour(TextButton::buttonOnColourId, Colours::darkgrey);
    addAndMakeVisible (&clearButton);
    clearButton.addListener (this);
    
    reverseButton.setButtonText ("Reverse");
    addAndMakeVisible (&reverseButton);
    reverseButton.addListener (this);
    
    for(int i = 0; i < 4; i++)
    {
        loopPoint[i].setButtonText((String)i);
        loopPoint[i].addListener(this);
        addAndMakeVisible(loopPoint[i]);
    }
}

LooperUI::~LooperUI()
{
}

void LooperUI::paint (Graphics& g)
{
    g.fillAll (Colours::lightcyan);
    g.setColour(Colours::black);
    g.drawRect(getLocalBounds());
}

void LooperUI::resized()
{
    Rectangle<int> r = getLocalBounds();
    float width = getWidth();
    
    Rectangle<int> bottomStrip = r.removeFromBottom(30);
    
    for (int i = 0; i < 4; i++)
    {
        loopPoint[i].setBounds(bottomStrip.removeFromLeft(width*0.25));
    }
    
    loadButton.setBounds(r.removeFromLeft(width * 0.1));
    saveButton.setBounds(r.removeFromLeft(width * 0.1));
    clearButton.setBounds(r.removeFromLeft(width * 0.1));
    reverseButton.setBounds(r.removeFromLeft(width * 0.1));
    playButton.setBounds (r.removeFromLeft((width - (width * 0.5)) * 0.5));
    recordButton.setBounds (r);
}

void LooperUI::buttonClicked (Button *button)
{
    if (button == &playButton)
    {
        looper.setPlayState (!looper.getPlayState());
        playButton.setToggleState (looper.getPlayState(), dontSendNotification);
        if (looper.getPlayState())
            playButton.setButtonText ("Stop");
        else
            playButton.setButtonText ("Play");
    }
    else if (button == &recordButton)
    {
        looper.setRecordState (!looper.getRecordState());
        recordButton.setToggleState (looper.getRecordState(), dontSendNotification);
    }
    else if (button == &saveButton)
    {
        if(!looper.getPlayState())
        {
            looper.recordBuffer();
        }
        else
        {
            AlertWindow::showMessageBoxAsync (AlertWindow::WarningIcon, "Error",
                                              "Stop playback before trying to save",
                                              "OK", this);
        }
    }
    else if(button == &clearButton)
    {
        looper.clearBuffer();
    }
    else if (button == &loadButton)
    {
        looper.loadFile();
    }
    else if (button == &reverseButton)
    {
        looper.reverseBuffer();
    }
    else if (button == &loopPoint[0])
    {
        looper.setLoopPoints(0);
    }
    else if (button == &loopPoint[1])
    {
        looper.setLoopPoints(1);
    }
    else if (button == &loopPoint[2])
    {
        looper.setLoopPoints(2);
    }
    else if (button == &loopPoint[3])
    {
        looper.setLoopPoints(3);
    }
}