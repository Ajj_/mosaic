/*
  ==============================================================================

    LooperUI.h
    Created: 10 Oct 2016 8:41:04am
    Author:  Alex Jones

  ==============================================================================
*/

#ifndef LOOPERUI_H_INCLUDED
#define LOOPERUI_H_INCLUDED

#include "../../JuceLibraryCode/JuceHeader.h"
#include "Looper.h"

//==============================================================================
/*
*/
class LooperUI    : public Component,
                    public ButtonListener
{
public:
    LooperUI(Looper& looper_);
    ~LooperUI();
    
    void paint (Graphics&) override;
    void resized() override;
    
    void buttonClicked (Button *button) override;

private:
    Looper& looper;
    
    TextButton loopPoint[4];
    TextButton saveButton;
    TextButton loadButton;
    TextButton clearButton;
    TextButton playButton;
    TextButton recordButton;
    
    ToggleButton reverseButton;
    
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (LooperUI)
};


#endif  // LOOPERUI_H_INCLUDED
